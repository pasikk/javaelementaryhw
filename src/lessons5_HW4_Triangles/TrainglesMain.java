package lessons5_HW4_Triangles;

public class TrainglesMain {
    public static void main(String[] args) {
        Triangles [] trian = new Triangles[10];
        trian [0] = new Triangles(0, 0, 2, 2, -2, -2);
        trian [1] = new Triangles(0, 0, 10, 0, 0, 5);
        trian [2] = new Triangles(0, 0, 0, 10, 6, 5);
        trian [3] = new Triangles(1, 2.5, 1.5, 2.4, 5.3, 4.4);
        trian [4] = new Triangles(1, 2.5, 1.5, 2.4, 5.3, 4.4);
        trian [5] = new Triangles(1, 2.5, 1.5, 2.4, 5.3, 4.4);
        trian [6] = new Triangles(1, 2.5, 1.5, 2.4, 5.3, 4.4);
        trian [7] = new Triangles(2, 5, 14, 24, 53, 44);
        trian [8] = new Triangles(1, 2.5, 1.5, 2.4, 5.3, 4.4);
        trian [9] = new Triangles(1, 2, 3, -1, 2, 5);

        Triangles [] trianEquil = new Triangles[10]; //равносторонний
        Triangles [] trianIsos = new Triangles[10]; //равнобедренный
        Triangles [] trianRight = new Triangles[10]; // прямоугольный
        Triangles [] trianArbit = new Triangles[10]; //произвольный

        int equil = 0;
        int isos  = 0;
        int right = 0;
        int arbit = 0;


        for (int i = 0; i < trian.length; i++){
            if (trian [i].typeEquil(i)) {
                equil = i + 1;
                trianEquil [i] = trian [i];
            }
            else if (trian [i].typeIsos(i)) {
                trianIsos [i] = trian [i];
                isos = i + 1;
            }
            else if (trian [i].typeRight(i)) {
                trianRight [i] = trian [i];
                right = i + 1;
            }
            else {
                trianArbit [i] = trian [i];
            }
        }

        System.out.println("Равносторонних треугольников " + equil);
        System.out.println("Равнобедренных треугольников " + isos);
        System.out.println("Прямоугольных треугольников " + right);
        System.out.println("Произвольных треугольников " + arbit);

        for (int i =0; i < trianEquil.length; i++){
           trianEquil [i].areaTriangle();
           trianEquil [i] = trianEquil [i];
        }
        for (int i =0; i < trianEquil.length; i++){
           trianEquil [i].perimetrTr();
           trianEquil [i] = trianEquil [i];
        }
    }
}

