package lessons5_HW4_Triangles;

public class Triangles {

    double sideAB;
    double sideBC;
    double sideCA;
    double perim;

    Triangles (double coorAX, double coorAY, double coorBX, double coorBY, double coorCX, double coorCY){
    /*    aX = coorAX;
        aY = coorAY;
        bX = coorBX;
        bY = coorBY;
        cX = coorCX;
        cY = coorCY;*/
        sideAB = Math.sqrt(Math.pow((coorBX - coorAX), 2) + Math.pow((coorBY - coorAY), 2));
        sideBC = Math.sqrt(Math.pow((coorCX - coorBX), 2) + Math.pow((coorCY - coorBY), 2));
        sideCA = Math.sqrt(Math.pow((coorAX - coorCX), 2) + Math.pow((coorAY - coorCY), 2));
    }


    double areaTriangle (){
        double area = Math.sqrt(perim / 2 * (perim / 2 - sideAB) * (perim / 2 - sideBC) * (perim / 2 - sideCA));
        return area;
    }

    double perimetrTr () {
        perim = sideAB + sideBC + sideCA;
        return perim;
    }

    boolean typeEquil (int i) {

        return sideAB == sideBC && sideBC == sideCA && sideCA == sideAB;
    }
    boolean typeIsos (int i) {

        return sideAB == sideBC || sideBC == sideCA || sideCA == sideAB;
    }
    boolean typeRight (int i) {

        return Math.sqrt(sideAB) == Math.sqrt(sideBC) + Math.sqrt(sideCA) || Math.sqrt(sideCA) == Math.sqrt(sideBC) + Math.sqrt(sideAB) || Math.sqrt(sideBC) == Math.sqrt(sideAB) + Math.sqrt(sideCA);
    }
}

