package lessons6_HW5;

public class Car {
    private int price;
    private int fuel;
    private int maxSpeed;

    public Car (int price, int fuel, int maxSpeed) {
        this.price = price;
        this.fuel = fuel;
        this.maxSpeed = maxSpeed;
    }

    public Car (int price, int maxSpeed) {
        this(price, 0, maxSpeed);
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getFuel() {
        return fuel;
    }

    public void setFuel(int fuel) {
        this.fuel = fuel;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    @Override
    public String toString() {
        return "Car{" + "price=" + price + ", fuel=" + fuel + ", maxSpeed=" + maxSpeed + '}';
    }
}
