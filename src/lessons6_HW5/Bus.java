package lessons6_HW5;

public class Bus extends Car  {

    private int numberOfSeats;

    public Bus (int price, int fuel, int maxSpeed, int numberOfSeats) {
        super(price, fuel, maxSpeed);
        this.numberOfSeats = numberOfSeats;
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    public void setNumberOfSeats(int numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }

    @Override
    public String toString() {
        return "Bus{" + "numberOfSeats=" + numberOfSeats + ", price=" + getPrice() + ", fuel="
                + getFuel() + ", maxSpeed=" + getMaxSpeed() + '}';
    }
}
