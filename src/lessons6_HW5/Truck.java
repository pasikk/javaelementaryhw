package lessons6_HW5;

class Truck extends Car{

    private int cargo;

    public Truck(int price, int fuel, int maxSpeed, int cargo){
        super(price, fuel, maxSpeed);
        this.cargo = cargo;
    }

    public int getCargo () {
        return cargo;
    }

    public void setCargo(int cargo) {
        this.cargo = cargo;
    }

    @Override
    public String toString() {
        return "Truck{" + "cargo=" + cargo + ", price=" + getPrice() + ", fuel="
                + getFuel() + ", maxSpeed=" + getMaxSpeed() +'}';
    }
}