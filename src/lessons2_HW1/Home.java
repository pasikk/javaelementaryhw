package lessons2_HW1;

import java.util.Scanner;

public class Home {
    public static void main(String[] args) {
        Scanner userName = new Scanner(System.in);
        System.out.print("Hi. Write your name and press Enter: ");
        String Name = userName.nextLine();
        System.out.println("Hello " + Name + "!");
    }
}
