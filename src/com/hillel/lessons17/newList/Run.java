package com.hillel.lessons17.newList;

import java.util.Iterator;

public class Run {
    public static void main(String[] args) {
        List<Integer> list1 = new List<>();
        list1.addFirst(2);
        list1.remove(0);
        System.out.println(list1.size());


        List<String> list = new List<>(new String[]{"1", "2", "3"});
        System.out.println("initial -> " + list + "; empty -> " + list.isEmpty());
        list.addLast("4");
        list.addLast("5");
        System.out.println("add two last -> " + list);
        list.addFirst("a");
        list.addFirst("b");
        System.out.println("add two first -> " + list);
        list.add(4, "q");
        list.add(6, "p");
        System.out.println("add two by indexes: 'q' to 4 and 'p' to 6 -> " + list);
        list.removeFirst();
        list.removeLast();
        System.out.println("remove first and last -> " + list);
        list.remove(4);
        list.remove(2);
        System.out.println("remove by indexes 4 and 2 -> " + list);
        list.replace(0, 2);
        System.out.println("replace by indexes 0 and 2 -> " + list);
        list.replace(2, 4);
        System.out.println("replace by indexes 2 and 4 -> " + list);

        System.out.println("size -> " + list.size() + "; empty -> " + list.isEmpty());


        List<String> list2 = new List<>();
        System.out.println("initial -> " + list2 + " empty -> " + list2.isEmpty());
        list2.addLast("1");
        list2.addLast("2");
        list2.addLast("3");
        list2.addLast("4");
        list2.addLast("5");
        System.out.println("add five last -> " + list2);
        list2.addFirst("a");
        list2.addFirst("b");
        System.out.println("add two first -> " + list2);
        list2.add(4, "q");
        list2.add(6, "p");
        System.out.println("add two by indexes: 'q' to 4 and 'p' to 6 -> " + list2);
        list2.removeFirst();
        list2.removeLast();
        System.out.println("remove first and last -> " + list2);
        list2.remove(4);
        list2.remove(2);
        System.out.println("remove by indexes 4 and 2 -> " + list2);
        list2.replace(0, 2);
        System.out.println("replace by indexes 0 and 2 -> " + list2);
        list2.replace(2, 4);
        System.out.println("replace by indexes 2 and 4 -> " + list2);



        Iterator<String> iterator = list.iterator();

        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }
}
