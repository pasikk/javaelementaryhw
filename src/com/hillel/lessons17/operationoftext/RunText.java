package com.hillel.lessons17.operationoftext;

import com.hillel.lessons17.operationoftext.operation.OperationText;

import java.util.*;

public class RunText {
    public static void main(String[] args) {
        OperationText text = new OperationText();
        List<String> list = text.createListOfText();
        printWordsAndCount(text.toMap(list));
        text.writeText(text.reverse(text.getList()));
    }

    public static void printWordsAndCount(Map<String, Integer> map) {
        for (Map.Entry<String, Integer> pair : map.entrySet()) {
            System.out.println((pair) + " ");
        }
    }
}
