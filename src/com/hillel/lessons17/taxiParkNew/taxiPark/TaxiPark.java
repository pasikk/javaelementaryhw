package com.hillel.lessons17.taxiParkNew.taxiPark;

import com.hillel.lessons19.car.Car;

import java.util.List;
import java.util.stream.Collectors;

public class TaxiPark {

    private List<Car> cars;

    public TaxiPark(List<Car> cars) {
        this.cars = cars;
    }

//    public int price() {
//        return cars.stream().map(cars -> cars.getPrice()).sum();
//    }

    public int price() {
        return cars.stream().reduce(0, (price, nextCar) -> price += nextCar.getPrice(),
                (first, second) -> first + second);
    }

    public void sortByFuel() {
       cars.stream().sorted((a, b) -> a.getFuel() - b.getFuel()).collect(Collectors.toList())
               .forEach(System.out::println);
    }

    public List<Car> findCarsBySpeedRange(int minSpeed, int maxSpeed) {
        return cars.stream().filter(car -> (minSpeed <= car.getMaxSpeed() &&  maxSpeed >= car.getMaxSpeed()))
                .collect(Collectors.toList());
    }

    public List<Car> getCars() {
        return cars;
    }

    public void setCars(List<Car> cars) {
        this.cars = cars;
    }

    @Override
    public String toString() {
        return "TaxiPark{" +
                "cars=" + cars +
                '}';
    }

}
