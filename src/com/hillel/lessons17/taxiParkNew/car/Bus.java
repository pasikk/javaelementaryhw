package com.hillel.lessons17.taxiParkNew.car;


public class Bus extends Car {

    private int numberOfSeats;

    public Bus(String mark, String model, int price, int fuel, int maxSpeed, int numberOfSeats) {
        super(mark, model, fuel, maxSpeed, price);
        this.numberOfSeats = numberOfSeats;
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    public void setNumberOfSeats(int numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }

    @Override
    public String toString() {
        return "Bus{mark=" + getMark() + ", model=" + getModel() + ", numberOfSeats=" + numberOfSeats + ", price=" + getPrice() + ", fuel="
                + getFuel() + ", maxSpeed=" + getMaxSpeed() + '}';
    }

}
