package com.hillel.lessons17.taxiParkNew.car;

public class Car {

    private String mark;
    private String model;
    private int price;
    private int fuel;
    private int maxSpeed;

    public Car(String mark, String model, int price, int fuel, int maxSpeed) {
        this.mark = mark;
        this.model = model;
        this.price = price;
        this.fuel = fuel;
        this.maxSpeed = maxSpeed;
    }

    public String getMark() {
        return mark;
    }

    public String getModel() {
        return model;
    }

    public int getPrice() {
        return price;
    }

    public int getFuel() {
        return fuel;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setFuel(int fuel) {
        this.fuel = fuel;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    @Override
    public String toString() {
        return "Car{" +
                "mark='" + mark + '\'' +
                ", model='" + model + '\'' +
                ", price=" + price +
                ", fuel=" + fuel +
                ", maxSpeed=" + maxSpeed +
                '}';
    }
}
