package com.hillel.lessons17.taxiParkNew;

import com.hillel.lessons17.taxiParkNew.car.Bus;
import com.hillel.lessons17.taxiParkNew.car.Car;
import com.hillel.lessons17.taxiParkNew.car.Truck;
import com.hillel.lessons17.taxiParkNew.taxiPark.TaxiPark;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        List<Car> cars = new ArrayList<>();
        cars.add(new Bus("DAF","FA12",12000, 20, 100,  35));
        cars.add(new Bus("DAF","Sport",8000, 25, 80,  40));
        cars.add(new Bus("MAN", "127",9000, 17, 75,  65));
        cars.add(new Bus("MAN", "127",10000, 20, 60,  33));
        cars.add(new Bus("Scania", "Touring",7500, 15, 140,  45));
        cars.add(new Truck("MAN", "TGX",12000, 20, 100,  3500));
        cars.add(new Truck("MAN", "TGX119",8000, 25, 120,  4000));
        cars.add(new Truck("Volvo", "V1",15000, 30, 80,  6000));
        cars.add(new Truck("Volvo", "V5",9000, 17, 150,  6500));

        TaxiPark tp = new TaxiPark(cars);
        System.out.println("Price of Taxi Park = " + tp.price());
        System.out.println();

        tp.sortByFuel();
        System.out.println();

        System.out.println(tp.findCarsBySpeedRange(80, 110));
    }
}
