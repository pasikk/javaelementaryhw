package com.hillel.lessons18;

public class Runer {
    public static void main(String[] args) {
        String text = "This is the father test\n"+
                "short and stout\n"+
                "This is the mother\n"+
                "with children all about\n"+
                "This is the brother, tall as you can see\n"+
                "This is the sister,\n"+
                "with a dolly on her knee\n"+
                "This is the baby still to grow\n"+
                "This is the family all in a row.\n";
        RegularFunction reg = new RegularFunction();
        System.out.println(reg.numberWordsRepeated(text));
        System.out.println(reg.replace(text));
        reg.printText(text);
        System.out.println(reg.numberPunctuationMarksRepeated(text));

    }
}
