package com.hillel.lessons18;

import java.util.regex.*;

public class RegularFunction {

    public int numberWordsRepeated (String text) {
       Pattern pattern = Pattern.compile("[a-z]+");
       Matcher matcher = pattern.matcher(text);
       int number = 0;

       while (matcher.find()) {
           number++;
       }
       return number;
    }

    public String replace(String text) {
        Pattern pattern = Pattern.compile("m(\\w*)");
        Matcher matcher = pattern.matcher(text);
        String string = matcher.replaceAll("X");
        return string;
    }

    public void printText(String text) {
        String[] test = text.split("\\W");
        for (int i = 0; i < test.length; ++i) {
            if (test[i].length() > 1 && test[i].charAt(0) == test[i].charAt(test[i].length() - 1)) {
                System.out.println(test[i]);
            }
        }
    }

    public int numberPunctuationMarksRepeated (String text) {
        Pattern pattern = Pattern.compile("[.,?!]");
        Matcher matcher = pattern.matcher(text);
        int number = 0;

        while (matcher.find()) {
            number++;
        }
        return number;
    }
}
