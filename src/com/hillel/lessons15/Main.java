package com.hillel.lessons15;

import com.hillel.lessons15.car.Bus;
import com.hillel.lessons15.car.Car;
import com.hillel.lessons15.car.Truck;
import com.hillel.lessons15.taxipark.TaxiPark;

public class Main {
    public static void main(String[] args) {

        Car[] cars = { new Bus("MAN", "127", 15, 180, 1000, 45),
                new Bus("Mercedes-Benz", "Sprinter II", 10, 180, 1000, 50),
                new Truck("MAN", "TGX", 30, 150, 15000, 50),
                new Truck("Volvo", "V1", 30, 150, 15000, 50),
                new Bus("Scania", "Touring", 12, 220, 5000, 55)};

        TaxiPark tp = new TaxiPark(cars);
        System.out.println(tp);
        System.out.println();

        tp.sortByFuelConsumption();
        System.out.println(tp);
    }
}