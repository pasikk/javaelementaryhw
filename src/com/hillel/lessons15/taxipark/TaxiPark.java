package com.hillel.lessons15.taxipark;

import com.hillel.lessons15.car.Car;

import java.util.Arrays;
import java.util.Comparator;
import java.util.StringJoiner;

public class TaxiPark {
    private Car[] cars;

    public TaxiPark(Car[] cars) {
        this.cars = cars;
    }

    public void sortByFuelConsumption() {
         Arrays.sort(cars, fuelComparatorByRealization);
//         Arrays.sort(cars, fuelComparatorByLambda);
//         Arrays.sort(cars, Car::compareFuelToAnotherCar);
//         Arrays.sort(cars, this::compareFuelForTwoCars);
    }

    private Comparator<Car> fuelComparatorByRealization = new Comparator<Car>() {
        @Override
        public int compare(Car c1, Car c2) {
            return c1.getFuel() - c2.getFuel();
        }
    };

    private Comparator<Car> fuelComparatorByLambda = (c1, c2) -> c1.getFuel() - c2.getFuel();

    private int compareFuelForTwoCars(Car c1, Car c2) {
        return c1.getFuel() - c2.getFuel();
    }

    @Override
    public String toString() {
        StringJoiner sj = new StringJoiner(",\n");
        for (Car car : cars) {
            sj.add(car.toString());
        }
        return sj.toString();
    }

    public Car[] getCars() {
        return cars;
    }

    public void setCars(Car[] cars) {
        this.cars = cars;
    }
}
