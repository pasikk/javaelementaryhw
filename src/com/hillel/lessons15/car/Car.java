package com.hillel.lessons15.car;

public abstract class Car {
    private String mark;
    private String model;
    private int price;
    private int fuel;
    private int maxSpeed;

    public Car(String mark, String model, int price, int fuel, int maxSpeed) {
        this.mark = mark;
        this.model = model;
        this.price = price;
        this.fuel = fuel;
        this.maxSpeed = maxSpeed;
    }

    public abstract String toOutputFormat();

    public int compareFuelToAnotherCar(Car another) {
        return this.getFuel() - another.getFuel();
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getFuel() {
        return fuel;
    }

    public void setFuel(int fuel) {
        this.fuel = fuel;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }
}
