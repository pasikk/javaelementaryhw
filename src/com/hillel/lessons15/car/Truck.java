package com.hillel.lessons15.car;

public class Truck extends Car {

    private int cargo;

    public Truck(String mark, String model, int price, int fuel, int maxSpeed, int cargo){
        super(mark, model, price, fuel, maxSpeed);
        this.cargo = cargo;
    }

    public Truck(String[] carProperties) {
        this(carProperties[1], carProperties[2], Integer.parseInt(carProperties[3]), Integer.parseInt(carProperties[4]),
                Integer.parseInt(carProperties[5]), Integer.parseInt(carProperties[6]));
    }

    public int getCargo () {
        return cargo;
    }

    public void setCargo(int cargo) {
        this.cargo = cargo;
    }

    public String toOutputFormat() {
        return "Truck," + getMark() + "," + getModel() +  cargo + "," + getPrice() + ","
                + getFuel() + "," + getMaxSpeed();
    }

    @Override
    public String toString() {
        return "Truck{mark=" + getMark() + ", model=" + getModel() + "cargo=" + cargo + ", price=" + getPrice() + ", fuel="
                + getFuel() + ", maxSpeed=" + getMaxSpeed() +'}';
    }
}