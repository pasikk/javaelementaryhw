package com.hillel.homework8;

import java.util.Arrays;
import java.util.Scanner;


public class Runner {

	public static void main(String[] args) {

		Scanner scaner = new Scanner(System.in);
		System.out.println("Введите минимальное число");
		int minNumber = scaner.nextInt();
		System.out.println("Введите максимальное число");
		int maxNumber = scaner.nextInt();
		System.out.println("Введите число потоков");
		int threadsCount = scaner.nextInt();

		PrimeNumbersStorage storage = new PrimeNumbersStorage();
		Thread[] threads = new Thread[threadsCount];

		for (int i = 0; i < threads.length; i++) {
			int t = maxNumber / threadsCount;
			int minNum = 0;
			int maxNum = 0;

			if (i == 0) {
				maxNum = t;

				threads[i] = new Thread(new PrimeNumbersFinderThread(minNumber, maxNum, storage));
			}else {
				minNum = minNum + t + 1;
				maxNum = minNum + t;

				if (maxNum < maxNumber) {
					threads[i] = new Thread(new PrimeNumbersFinderThread(minNum, maxNum, storage));
				}else {
					threads[i] = new Thread(new PrimeNumbersFinderThread(minNum, maxNumber, storage));
				}
				minNum = maxNum;
			}
		}

		for (Thread thread : threads) {
			thread.start();
		}

		for (Thread thread : threads) {
			try {
				thread.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		System.out.println(Arrays.toString(storage.getNumbers()));

	}

}
