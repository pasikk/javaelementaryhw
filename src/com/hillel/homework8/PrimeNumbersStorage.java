package com.hillel.homework8;

public class PrimeNumbersStorage {
	private int[] numbers = {};

	synchronized void addNumbers(int[] numbersToAdd) {
		for (int i : numbersToAdd) {
			arrNumbers(i);
		}
	}

	private int[] arrNumbers(int i) {
		if (numbers.length == 0) {
			numbers = new int[]{i};
		} else {
			int[] tmp = new int[numbers.length + 1];

			for (int j = 0; j < numbers.length; j++) {
				tmp[j] = numbers[j];
			}
			tmp[numbers.length] = i;
			return tmp;
		}
		return numbers;
	}

	public int[] getNumbers() {
		return numbers;
	}

	public void setNumbers(int[] numbers) {
		this.numbers = numbers;
	}
}
