package com.hillel.homework8;

import com.hillel.homework8.PrimeNumbersStorage;
import java.util.Scanner;

public class PrimeNumbersFinderThread implements Runnable {

	private int minNumber;
	private int maxNumber;
	private int threadsCoun;
	private PrimeNumbersStorage storage;

	public PrimeNumbersFinderThread(int minNumber, int maxNumber, PrimeNumbersStorage storage) {
		this.minNumber = minNumber;
		this.maxNumber = maxNumber;
		this.storage = storage;
	}

	@Override
	public void run() {
		int[] findedPrimeNumbers = {};

		for (int i = getMinNumber(); i < getMaxNumber(); i++) {
			if (isPrime(i)) {
				findedPrimeNumbers = new int[i];

			}
			storage.addNumbers(findedPrimeNumbers);
		}
		getStorage().addNumbers(findedPrimeNumbers);
	}

	public void minNum() {
		Scanner scaner = new Scanner(System.in);
		minNumber = scaner.nextInt();
	}

	public void maxNum() {
		Scanner scaner = new Scanner(System.in);
		maxNumber = scaner.nextInt();
	}

	public void threadsCoun() {
		Scanner scaner = new Scanner(System.in);
		threadsCoun = scaner.nextInt();
	}

	private boolean isPrime(int num) {
		boolean t = true;

		for (int i = 2; i < num; i++) {
			if (num % i == 0) {
				t = false;
				break;
			}
		}
		return t;
	}

	public int getMinNumber() {
		return minNumber;
	}

	public void setMinNumber(int minNumber) {
		this.minNumber = minNumber;
	}

	public int getMaxNumber() {
		return maxNumber;
	}

	public void setMaxNumber(int maxNumber) {
		this.maxNumber = maxNumber;
	}

	public PrimeNumbersStorage getStorage() {
		return storage;
	}

	public void setStorage(PrimeNumbersStorage storage) {
		this.storage = storage;
	}

}
