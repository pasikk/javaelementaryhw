package com.hillel.homework9;

import com.hillel.homework9.model.Car;
import com.hillel.homework9.storage.CarsStorage;
import com.hillel.homework9.storage.file.FileCarsStorage;
import java.io.FileOutputStream;
import java.io.IOException;

public class Runner {
    public static void main(String[] args) throws IOException {

		CarsStorage storage = new FileCarsStorage("input.txt", "output.txt");
		Car[] cars = storage.readCars();

		storage.writeCars(cars);

    }
}
