package com.hillel.homework9.model;

public interface Car {
    int getPrice();

    void setPrice(int price);

    int getFuelConsumption();

    void setFuelConsumption(int fuelConsumption);

    int getMaxSpeed();

    void setMaxSpeed(int maxSpeed);

    String format ();
}
