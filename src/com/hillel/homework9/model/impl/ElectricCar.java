package com.hillel.homework9.model.impl;

import com.hillel.homework9.model.Car;

public class ElectricCar implements Car {

    private int price;
    private int maxSpeed;
    private int powerReserve;

    public ElectricCar(int price, int maxSpeed, int powerReserve) {
        this.price = price;
        this.maxSpeed = maxSpeed;
        this.powerReserve = powerReserve;
    }

    public ElectricCar(String[] carProperties) {
        this(Integer.parseInt(carProperties[1]), Integer.parseInt(carProperties[2]),
                Integer.parseInt(carProperties[3]));
    }

    @Override
    public int getPrice() {
        return price;
    }

    @Override
    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public int getFuelConsumption() {
        return 0;
    }

    @Override
    public void setFuelConsumption(int fuelConsumption) {
    }

    @Override
    public int getMaxSpeed() {
        return maxSpeed;
    }

    @Override
    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public int getPowerReserve() {
        return powerReserve;
    }

    public void setPowerReserve(int powerReserve) {
        this.powerReserve = powerReserve;
    }

    @Override
    public String format() {
        return "ElectricCar," + "," + getPrice() + "," + getMaxSpeed() + "," + powerReserve ;
    }

}