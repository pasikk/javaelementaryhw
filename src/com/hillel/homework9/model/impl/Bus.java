package com.hillel.homework9.model.impl;

import com.hillel.homework9.model.Car;

public class Bus implements Car {

    private int price;
    private int fuelConsumption;
    private int maxSpeed;
    private int numberOfSeats;

    public Bus(int price, int fuelConsumption, int maxSpeed, int numberOfSeats) {
        this.price = price;
        this.fuelConsumption = fuelConsumption;
        this.maxSpeed = maxSpeed;
        this.numberOfSeats = numberOfSeats;
    }

    public Bus(String[] carProperties) {
        this(Integer.parseInt(carProperties[1]), Integer.parseInt(carProperties[2]),
                Integer.parseInt(carProperties[3]), Integer.parseInt(carProperties[4]));
    }

    @Override
    public int getPrice() {
        return price;
    }

    @Override
    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public int getFuelConsumption() {
        return fuelConsumption;
    }

    @Override
    public void setFuelConsumption(int fuelConsumption) {
        this.fuelConsumption = fuelConsumption;
    }

    @Override
    public int getMaxSpeed() {
        return maxSpeed;
    }

    @Override
    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    public void setNumberOfSeats(int numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }

    @Override
    public String format() {
        return "Bus," + "," + getPrice() + ","
                + getFuelConsumption() + "," + getMaxSpeed() + "," + numberOfSeats ;
    }

}

