package com.hillel.homework9.taxipark;

import com.hillel.homework9.model.Car;

public interface TaxiPark {

    int calculateCost();

    void sortCarsByFuelConsumption();

    Car[] findCarsBySpeedRange(int minSpeed, int maxSpeed);

    Car[] getCars();

}
