package com.hillel.homework9.storage.file;

import com.hillel.homework9.model.Car;
import com.hillel.homework9.model.impl.Bus;
import com.hillel.homework9.model.impl.ElectricCar;
import com.hillel.homework9.model.impl.Truck;
import com.hillel.homework9.storage.CarsStorage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.StringJoiner;

public class FileCarsStorage implements CarsStorage {

    private String inputFile;
    private String outputFile;

    public FileCarsStorage(String inputFile, String outputFile) {
        this.inputFile = inputFile;
        this.outputFile = outputFile;
    }

    @Override
    public Car[] readCars() {

        FileInputStream read = null;
        byte[] fileBytes = new byte[0];

        try {

            read = new FileInputStream(inputFile);

            fileBytes = new byte[read.available()];

            read.read(fileBytes);

        } catch (IOException e) {

            e.printStackTrace();

        }

        String fileString = new String(fileBytes);

        String[] carStrings = fileString.split(";" + System.lineSeparator());

        Car[] cars = new Car[carStrings.length];

        for (int i = 0; i < carStrings.length; i++) {
            String[] carProperties = carStrings[i].split(",");
            switch (carProperties[0]) {
                case "Bus":
                    cars[i] = new Bus(Integer.parseInt(carProperties[1]), Integer.parseInt(carProperties[2]),
                            Integer.parseInt(carProperties[3]), Integer.parseInt(carProperties[4]));
                    break;
                case "Truck":
                    cars[i] = new Truck(Integer.parseInt(carProperties[1]), Integer.parseInt(carProperties[2]),
                            Integer.parseInt(carProperties[3]), Integer.parseInt(carProperties[4]));
                    break;
                case "ElectricCar":
                    cars[i] = new ElectricCar(Integer.parseInt(carProperties[1]), Integer.parseInt(carProperties[2]),
                            Integer.parseInt(carProperties[3]));
                    break;
            }
        }

        return cars;
    }

    @Override
    public void writeCars(Car[] carsToWrite) {
        try (FileOutputStream write = new FileOutputStream(outputFile)) {
            StringJoiner sj = new StringJoiner(";" + System.lineSeparator());
            for (Car car : carsToWrite) {
                sj.add(car.format());
            }
            write.write(sj.toString().getBytes());


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getInputFile() {
        return inputFile;
    }

    public void setInputFile(String inputFile) {
        this.inputFile = inputFile;
    }

    public String getOutputFile() {
        return outputFile;
    }

    public void setOutputFile(String outputFile) {
        this.outputFile = outputFile;
    }

}