package com.hillel.homework9.storage;

import com.hillel.homework9.model.Car;

public interface CarsStorage {

    Car[] readCars();

    void writeCars(Car[] carsToWrite);
}