package com.hillel.homework7.flower;

public interface Flower {

    String getNameFlower();
    void setNameFlower(String nameFlower);

    double getPriceFlower();
    void setPriceFlower(double priceFlower);

    double getLengthFlower();
    void setLengthFlower(double lengthFlower);

    int getDaysFresh();
    void setDaysFresh(int daysFresh);
}
