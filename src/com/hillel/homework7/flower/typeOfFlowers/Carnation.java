package com.hillel.homework7.flower.typeOfFlowers;

import com.hillel.homework7.flower.FlowerImpl;

public class Carnation extends FlowerImpl {

    public Carnation() {
        super("Carnation", 30, 0.30, 3);
    }
}
