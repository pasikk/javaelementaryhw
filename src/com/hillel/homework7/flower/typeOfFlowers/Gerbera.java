package com.hillel.homework7.flower.typeOfFlowers;

import com.hillel.homework7.flower.FlowerImpl;

public class Gerbera extends FlowerImpl {

    public Gerbera() {
        super("Gerbera", 35, 0.50, 6);
    }
}
