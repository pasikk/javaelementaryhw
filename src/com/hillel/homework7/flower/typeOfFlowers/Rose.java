package com.hillel.homework7.flower.typeOfFlowers;

import com.hillel.homework7.flower.FlowerImpl;

public class Rose extends FlowerImpl {

    public Rose() {
        super("Rose", 40, 0.90, 5);
    }
}
