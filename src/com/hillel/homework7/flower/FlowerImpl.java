package com.hillel.homework7.flower;

public class FlowerImpl implements Flower{
    private String nameFlower;
    private double priceFlower;
    private double lengthFlower;
    private int daysFresh;

    public FlowerImpl(String nameFlower, double priceFlower, double lengthFlower, int daysFresh) {
        this.nameFlower = nameFlower;
        this.priceFlower = priceFlower;
        this.lengthFlower = lengthFlower;
        this.daysFresh = daysFresh;
    }

    @Override
    public String toString() {
        return "FlowerImpl{" +
                "nameFlower='" + nameFlower + '\'' +
                ", priceFlower=" + priceFlower +
                ", lengthFlower=" + lengthFlower +
                ", daysFresh=" + daysFresh +
                '}';
    }

    @Override
    public String getNameFlower() {
        return nameFlower;
    }

    @Override
    public void setNameFlower(String nameFlower) {
        this.nameFlower = nameFlower;
    }

    @Override
    public double getPriceFlower() {
        return priceFlower;
    }

    @Override
    public void setPriceFlower(double priceFlower) {
        this.priceFlower = priceFlower;
    }

    @Override
    public double getLengthFlower() {
        return lengthFlower;
    }

    @Override
    public void setLengthFlower(double lengthFlower) {
        this.lengthFlower = lengthFlower;
    }

    @Override
    public int getDaysFresh() {
        return daysFresh;
    }

    @Override
    public void setDaysFresh(int daysFresh) {
        this.daysFresh = daysFresh;
    }
}
