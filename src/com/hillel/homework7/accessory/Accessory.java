package com.hillel.homework7.accessory;

public interface Accessory {
    String getNameAccessory();
    void setNameAccessory(String nameAccessory);

    double getPriceAccessory();
    void setPriceAccessory(double priceAccessory);
}
