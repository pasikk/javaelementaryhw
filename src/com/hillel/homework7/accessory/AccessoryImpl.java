package com.hillel.homework7.accessory;

public class AccessoryImpl implements Accessory {
    private String nameAccessory;
    private double priceAccessory;

    public AccessoryImpl(String nameAccessory, double priceAccessory){
        this.nameAccessory = nameAccessory;
        this.priceAccessory = priceAccessory;
    }

    @Override
    public String toString() {
        return "AccessoryImpl{" +
                "nameAccessory='" + nameAccessory + '\'' +
                ", priceAccessory=" + priceAccessory +
                '}';
    }

    @Override
    public String getNameAccessory() {
        return nameAccessory;
    }

    @Override
    public void setNameAccessory(String nameAccessory) {
        this.nameAccessory = nameAccessory;
    }

    @Override
    public double getPriceAccessory() {
        return priceAccessory;
    }

    @Override
    public void setPriceAccessory(double priceAccessory) {
        this.priceAccessory = priceAccessory;
    }
}
