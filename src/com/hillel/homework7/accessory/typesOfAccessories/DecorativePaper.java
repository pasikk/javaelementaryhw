package com.hillel.homework7.accessory.typesOfAccessories;

import com.hillel.homework7.accessory.AccessoryImpl;

public class DecorativePaper extends AccessoryImpl {

    public DecorativePaper() {
        super("DecorativePaper", 5);
    }
}
