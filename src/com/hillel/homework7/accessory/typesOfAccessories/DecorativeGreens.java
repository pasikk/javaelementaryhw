package com.hillel.homework7.accessory.typesOfAccessories;

import com.hillel.homework7.accessory.AccessoryImpl;

public class DecorativeGreens extends AccessoryImpl {

    public DecorativeGreens() {
        super("DecorativeGreens", 12.5);
    }
}
