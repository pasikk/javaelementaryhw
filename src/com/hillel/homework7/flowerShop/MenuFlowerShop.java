package com.hillel.homework7.flowerShop;

import com.hillel.homework7.accessory.AccessoryImpl;
import com.hillel.homework7.accessory.typesOfAccessories.ColorRibbon;
import com.hillel.homework7.accessory.typesOfAccessories.DecorativeGreens;
import com.hillel.homework7.accessory.typesOfAccessories.DecorativePaper;
import com.hillel.homework7.bouquet.BouquetImpl;
import com.hillel.homework7.exception.FiendException;
import com.hillel.homework7.flower.FlowerImpl;
import com.hillel.homework7.flower.typeOfFlowers.Carnation;
import com.hillel.homework7.flower.typeOfFlowers.Gerbera;
import com.hillel.homework7.flower.typeOfFlowers.Rose;

import java.lang.module.FindException;
import java.util.Scanner;

public class MenuFlowerShop {

    private BouquetImpl bouquet = new BouquetImpl();
    private boolean exit;
    private int push = pushUser();

    private int pushUser() {
        Scanner scanner = new Scanner(System.in);
        int temp = scanner.nextInt();

        return temp;
    }

    void mainMenu() {
        System.out.println("Push 1 - Create bouquet");
        System.out.println("Push 2 - Bouquets operations");
        System.out.println("Push 3 - Print current bouquet");
        System.out.println("Push 0 - Exit");
        do {


            switch (push) {
                case 1:
                    push = menuAddProducs(push);
                    break;
                case 2:
                    push = menuOperations(push);
                    break;
                case 3:
                    menuPrint();
                    break;
                case 0:
                    exit = true;
                    break;
            }
        }while (!exit);
    }

    public int menuAddProducs(int push) {
        push = pushUser();

        System.out.println("Push 1 - Add flower");
        System.out.println("Push 2 - Add accessory");
        System.out.println("Push 0 - Exit");

        switch (push){
            case 1:
                push = addFlower(push);
                break;
            case 2:
                push = addAccessory(push);
                break;
            case 0:
                break;
        }
        return push;
    }

    public int menuOperations(int push) {
        push = pushUser();

        System.out.println("Push 1 - Sort flowers by day to live");
        System.out.println("Push 2 - Find flowers by stem length");
        System.out.println("Push 3 - Price of bouquet");
        System.out.println("Push 4 - Add flower");
        System.out.println("Push 5 - Add accessory");
        System.out.println("0 - Exit");

        switch (push){
            case 1:
                FlowerImpl[] flowers;
                flowers = bouquet.sortFresh();
//                for (FlowerImpl flower : flowers){
                    System.out.println(flowers);
//                }
                break;
            case 2:
                try {
                    FlowerImpl[] flowersL;
                    Scanner scanner = new Scanner(System.in);
                    System.out.println("Input max length");
                    int maxL = scanner.nextInt();
                    System.out.println("Input min length");
                    int minL = scanner.nextInt();
                    flowersL = bouquet.fiendLengthFlower(minL, maxL);
                    System.out.println(flowersL);
                }catch (FindException e) {
                    System.out.println(e);
                } catch (FiendException e) {
                    e.printStackTrace();
                }
                break;
            case 3:
                System.out.println("Prise of bouquet: " + bouquet.priseBouquet());
                break;
            case 4:
                push = addFlower(push);
                break;
            case 5:
                push = addAccessory(push);
                break;
            case 0:
                break;
        }
        return push;
    }

    public void menuPrint(){
        String printBouquet = bouquet.printBouquet(bouquet.getFlowers(), bouquet.getAccessories());
        System.out.println(printBouquet);
    }

    public int addFlower(int push) {
        push = pushUser();

        System.out.println("Select flowers:");
        System.out.println("Push 1 - Carnation");
        System.out.println("Push 2 - Gerbera");
        System.out.println("Push 3 - Rose");
        System.out.println("Push 0 - Exit");

        switch (push){
            case 1:
                FlowerImpl carnation = new Carnation();
                bouquet.addToFlower(carnation);
                break;
            case 2:
                FlowerImpl gerbera = new Gerbera();
                bouquet.addToFlower(gerbera);
                break;
            case 3:
                FlowerImpl rose = new Rose();
                bouquet.addToFlower(rose);
                break;
            case 0:
                break;
        }
        return push;
    }

    public int addAccessory(int push) {
        push = pushUser();

        System.out.println("Select accessory:");
        System.out.println("Push 1 - Color Ribbon");
        System.out.println("Push 2 - Decorative Greens");
        System.out.println("Push 3 - Decorative Paper");
        System.out.println("Push 0 - Exit");

        switch (push){
            case 1:
                AccessoryImpl colorRibbon = new ColorRibbon();
                bouquet.addToAcAccessory(colorRibbon);
                break;
            case 2:
                AccessoryImpl decorativeGreens = new DecorativeGreens();
                bouquet.addToAcAccessory(decorativeGreens);
                break;
            case 3:
                AccessoryImpl decorativePaper = new DecorativePaper();
                bouquet.addToAcAccessory(decorativePaper);
                break;
            case 0:
                break;
        }
        return push;
    }
}
