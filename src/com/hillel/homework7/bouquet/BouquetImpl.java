package com.hillel.homework7.bouquet;

import com.hillel.homework7.accessory.Accessory;
import com.hillel.homework7.accessory.AccessoryImpl;
import com.hillel.homework7.exception.FiendException;
import com.hillel.homework7.flower.Flower;
import com.hillel.homework7.flower.FlowerImpl;

public class BouquetImpl implements Bouquet {

    FlowerImpl[] flowers = new FlowerImpl[0];
    AccessoryImpl[] accessories;

    @Override
    public void addToFlower(FlowerImpl flower) {
        flowers = addFlowerArray(flowers, flower);
    }

    private FlowerImpl[] addFlowerArray(FlowerImpl[] flower, FlowerImpl flowerToAdd) {
        FlowerImpl[] newFlowerArr = new FlowerImpl[flower.length + 1];

        for (int i = 0; i < flower.length; i++) {
            newFlowerArr[i] = flower[i];
        }
        newFlowerArr[flower.length] = flowerToAdd;
        return newFlowerArr;
    }

    @Override
    public void addToAcAccessory(AccessoryImpl accessory) {
        accessories = addAccessoryArray(accessories, accessory);
    }

    private AccessoryImpl [] addAccessoryArray(AccessoryImpl[] accses, AccessoryImpl accsesToAdd) {
        AccessoryImpl[] newAccsesArr = new AccessoryImpl[accses.length + 1];

        for (int i = 0; i < accses.length; i++) {
            newAccsesArr[i] = accses[i];
        }
        newAccsesArr[accses.length] = accsesToAdd;
        return newAccsesArr;
    }

    @Override
    public double priseBouquet() {
        double prise = 0;

        for (FlowerImpl flower : getFlowers())
            prise += flower.getPriceFlower();

        for (AccessoryImpl accessory : getAccessories())
            prise += accessory.getPriceAccessory();
        return prise;
    }

    @Override
    public FlowerImpl[] sortFresh() {
        for (int i = 0; i < flowers.length; i++) {
            for (int j = 0; j < flowers.length; j++) {
                if (flowers[j].getDaysFresh() < flowers[i].getDaysFresh()) {
                    FlowerImpl temp = flowers[j];
                    flowers[j] = flowers[i];
                    flowers[i] = temp;
                }
            }
        }
        return flowers;
    }

    @Override
    public FlowerImpl[] fiendLengthFlower(int minLength, int maxLength) throws FiendException {
        if (minLength < 0 || maxLength < 0) {
            throw new FiendException();
        }

        FlowerImpl[] temp = new FlowerImpl[0];

        for (FlowerImpl flower : flowers) {
            if (flower.getLengthFlower() >= minLength && flower.getLengthFlower() <= maxLength) {
                temp = addFlowerArray(temp, flower);
            }
        }
        return temp;
    }

    public String printBouquet(FlowerImpl[] flowers, AccessoryImpl[] accessories) {
        String temp = null;

        for (FlowerImpl flower1 : flowers) {
            temp += (flower1.toString() + "\n");
        }

        for (AccessoryImpl acces : accessories) {
            temp += (acces.toString() + "\n");
        }
        return temp;
    }

    public FlowerImpl[] getFlowers() {
        return flowers;
    }

    public AccessoryImpl[] getAccessories() {
        return accessories;
    }

}