package com.hillel.homework7.bouquet;

import com.hillel.homework7.accessory.AccessoryImpl;
import com.hillel.homework7.exception.FiendException;
import com.hillel.homework7.flower.FlowerImpl;

public interface Bouquet {

    void addToFlower(FlowerImpl flower);

    void addToAcAccessory(AccessoryImpl accessory);

    double priseBouquet();

    FlowerImpl[] sortFresh();

    FlowerImpl[] fiendLengthFlower(int minLength, int maxLength) throws FiendException;
}
