package lessons3_HW2;
//Вывести числа от 1 до k в виде матрицы N x N слева направо и сверху вниз

public class MatrixN_N {
    public static void main(String[] args) {
        int primNumber = 25;
        int n = (int) Math.sqrt(primNumber);
        int[][] reskArray = new int[n][n];
        int number = 0;

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                number++;
                reskArray[i][j] = number;
                System.out.print(reskArray[i][j] + " ");
            }
            System.out.println();
        }
    }
}
