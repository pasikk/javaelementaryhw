package lessons3_HW2;
//1. Объявить массив с n-м количеством случайных чисел (явно инициализировать массив) и выбрать из него четные и нечетные числа.

public class EvenNotEvenNumbers {
    public static void main(String[] args) {
        int[] Numbers = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        for (int i : Numbers){
            if (i % 2 == 0) {
                System.out.println("Четное число " + i);
            }
        }
        for (int i : Numbers){
            if (i % 2 != 0) {
                System.out.println("Нечетное число " + i);
            }
        }
    }
}
