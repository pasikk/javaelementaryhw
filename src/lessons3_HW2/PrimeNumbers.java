package lessons3_HW2;
//Объявить массив с n-м количеством случайных чисел (явно инициализировать массив) и выбрать из него простые числа.

public class PrimeNumbers {
    public static void main(String[] args) {
        int[] Numbers = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        boolean Number = true;
        for (int prNumb : Numbers){
            for (int j = 2; j < prNumb; j++){
                if (prNumb % j == 0){
                    Number = false;
                    break;
                }
            }
            if (Number) {
                System.out.println("Простое число: " + prNumb);
            }
            else {
                Number = true;
            }
        }
    }
}
