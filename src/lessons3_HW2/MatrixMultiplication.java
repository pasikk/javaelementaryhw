package lessons3_HW2;
//Умножить две матрицы

public class MatrixMultiplication {
    public static void main(String[] args) {
        int[][] matrix1 = {
                {1, 5, 6},
                {2, 1, 3},
                {7, 9, 8},
                {6, 2, 9},
                {5, 4, 1}};
        int[][] matrix2 = {
                {7, 5, 2, 1},
                {2, 9, 8, 3},
                {3, 6, 9, 8}};
        int a = matrix1.length;
        int b = matrix2[0].length;
        int c = matrix2.length;
        int[][] resultatMat = new int[a][b];

        for (int i = 0; i < a; i++) {
            for (int j = 0; j < b; j++)
                for (int t = 0; t < c; t++) {
                    resultatMat [i][j] += matrix1 [i][t] * matrix2 [t][j];
                }
        }
        for (int i = 0; i < resultatMat.length; i++) {
            for (int j = 0; j < resultatMat[0].length; j++) {
                System.out.format("%6d ", resultatMat[i][j]);
            }
            System.out.println();
        }
    }
}
