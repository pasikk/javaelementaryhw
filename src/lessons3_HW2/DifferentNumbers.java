package lessons3_HW2;
//Задать массив с n чисел. Найти числа, состоящее только из различных цифр.

public class DifferentNumbers {
    public static void main(String[] args) {
        int[] arrNumb = {1212, 1234, 4565, 5656, 6788, 9903, 8101, 777};
        for (int elem : arrNumb){
            if (isDistinct(elem)){
                System.out.println(elem);
            }
        }
    }
    private static boolean isDistinct(int x) {
        int [] nums = new int[10];
        int tmpIndx;
        while (x != 0) {
            tmpIndx = x % 10;
            if(nums[tmpIndx] == 1)
                return false;
            nums[tmpIndx]++;
            x /=10;
        }
        return true;
    }
}
