package lessons3_HW2;
//Задать массив с n чисел. Найти самое короткое и самое длинное число. Вывести найденные числа и их длину.

public class SmallHighestNumbers {
    public static void main(String[] args) {
        int[] arrNumbers = {50, 470, 30, 400, 502, 60, 71, 81, 51, 140, 191, 362, 202, 546};
        int lengthMax = 0;
        int lengthMin = 0;
        int result = arrNumbers [0];
        for (int i = 1; i < arrNumbers.length; i++) {
            if (arrNumbers[i] > result) {
                result = arrNumbers[i];
                lengthMax = (int)(Math.log10(result)+1);
            }
        }
        System.out.println("Максимальное число в массиве: " + result + " Состоит из " + lengthMax + " символов");
        for (int i = 1; i < arrNumbers.length; i++) {
            if (arrNumbers[i] < result) {
                result = arrNumbers[i];
                lengthMin = (int)(Math.log10(result)+1);
            }
        }
        System.out.println("Минимальное число в массиве: " + result + " Состоит из " + lengthMin + " символов");
    }
}
