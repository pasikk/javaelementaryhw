package lessons3_HW2;
/*Создайте массив из 20-ти первых чисел Фибоначчи и выведите его на экран
(первый и второй члены последовательности равны единицам, а каждый следующий — сумме двух предыдущих)*/

public class FibonacciNumbers {
    public static void main(String[] args) {
        int[] Fibonacci = new int[20];
        System.out.print("Числа Фибоначи: ");
        for (int i = 0 ; i < Fibonacci.length; i++){
            if (i < 2){
                Fibonacci [i] = 1;
            }
            else {
                Fibonacci [i] = Fibonacci [i - 2] + Fibonacci [i - 1];
            }
            System.out.print(Fibonacci [i] + " ");
        }
    }
}
