package lessons5_HW4_Recursion;

public class Recursion {
    public static void main(String[] args) {
        FineRecursion vol = new FineRecursion(0);
        vol.printReursion(6);

        System.out.println();

        TheSumRecursion sum = new TheSumRecursion(0);
        sum.printReursion(5);
        System.out.println(sum.result);
    }
}