package lessons5_HW4_Recursion;

public class FineRecursion {
    int number;

    FineRecursion(int i) {
        number = i;
    }

    void printReursion (int i) {
        if (i == 0){
            return;
        }
        else {
            printReursion(i - 1);
            System.out.print(i + " ");
        }
    }
}
