package lessons5_HW4_Recursion;

public class TheSumRecursion {
    int number;
    int result;

    TheSumRecursion(int i) {
        number = i;
    }

    void printReursion (int i) {
        if (i == 0){
            return;
        }
        else {
            printReursion(i - 1);
            result += i;
        }
    }
}
