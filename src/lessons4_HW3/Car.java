package lessons4_HW3;

public class Car {

    public int id;
    public String mark;
    public String brand;
    public int yearCar;
    public String color;
    public int price;
    public int regNumb;

    public Car(int id, String mark, String brand, int yearCar, String color, int price, int regNumb) {
        this.id = id;
        this.mark = mark;
        this.brand = brand;
        this.yearCar = yearCar;
        this.color = color;
        this.price = price;
        this.regNumb = regNumb;
    }

    public int getId() {
        return id;
    }
    public String getMark() {
        return mark;
    }
    public String getBrand() {
        return brand;
    }
    public int getYearCar() {
        return yearCar;
    }
    public String getColor() {
        return color;
    }
    public int getPrice() {
        return price;
    }
    public int getRegNumb() {
        return regNumb;
    }

}
