package lessons4_HW3;

public class CarConstruktion extends Car {

    public CarConstruktion(int id, String mark, String brand, int yearCar, String color, int price, int regNumb) {
        super(id, mark, brand, yearCar, color, price, regNumb);
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public void getBrand(String brand) {
        this.brand = brand;
    }

    public void getYearCar(int yearCar) {
        this.yearCar = yearCar;
    }

    public void getColor(String color) {
        this.color = color;
    }

    public void getPrice(int price) {
        this.price = price;
    }

    public void getRegNumb(int regNumb) {
        this.regNumb = regNumb;
    }

    public void presetMark() {
        if ((this.getMark()).equals("BMW")) {
            System.out.println("id: " + getId());
            System.out.println("Марка: " + getMark());
            System.out.println("Модель: " + getBrand());
            System.out.println("Год выпуска: " + getYearCar());
            System.out.println("Цвет: " + getColor());
            System.out.println("Цена: " + getPrice());
            System.out.println("Регистрационный номер: " + getRegNumb());
            System.out.println();
        }
    }

    public void presetMarYear() {
        int y = 2019;
        if ((this.getMark()).equals("BMW") && ((y - this.getYearCar()) > 5)) {
            System.out.println("id: " + getId());
            System.out.println("Марка: " + getMark());
            System.out.println("Модель: " + getBrand());
            System.out.println("Год выпуска: " + getYearCar());
            System.out.println("Цвет: " + getColor());
            System.out.println("Цена: " + getPrice());
            System.out.println("Регистрационный номер: " + getRegNumb());
            System.out.println();
        }
    }

    public void priceCar() {
        int y = 2001;
        if ((getYearCar() == y) && (this.getPrice() > 18000)) {
            System.out.println("id: " + getId());
            System.out.println("Марка: " + getMark());
            System.out.println("Модель: " + getBrand());
            System.out.println("Год выпуска: " + getYearCar());
            System.out.println("Цвет: " + getColor());
            System.out.println("Цена: " + getPrice());
            System.out.println("Регистрационный номер: " + getRegNumb());
            System.out.println();
        }
    }
}
