package lessons4_HW3;

class CarMain {

    public static void main(String[] args) {
        CarConstruktion[] cars = new CarConstruktion[10];
        cars [0] = new CarConstruktion(1, "BMW", "X5", 2015, "Черный", 20000, 5116);
        cars [1] = new CarConstruktion(2, "Toyota", "Impreza", 2001, "Белый", 25000, 2232);
        cars [2] = new CarConstruktion(3, "BMW", "Buster", 2000, "Красный", 19000, 4685);
        cars [3] = new CarConstruktion(4, "Nissan", "626", 1999, "Жельый", 10000, 1642);
        cars [4] = new CarConstruktion(5, "Peggo", "7", 2018, "Синий", 23500, 3546);
        cars [5] = new CarConstruktion(6, "Chtevrolet", "Aveo", 2017, "Черный", 21000, 1584);
        cars [6] = new CarConstruktion(7, "Opel", "725", 2015, "Вишневый", 30000, 1673);
        cars [7] = new CarConstruktion(8, "Lada", "99", 2001, "Серый", 15000, 9552);
        cars [8] = new CarConstruktion(9, "Hunday", "Tucson", 2001, "Белый", 19000, 5427);
        cars [9] = new CarConstruktion(10, "BMW", "1", 2001, "Красный", 20500, 4561);

        System.out.println("Список автомобилей марки BMW:");
        for (int i = 0; i < 10; i++) {
            cars[i].presetMark();
        }
        System.out.println("список автомобилей BMW модели, которые эксплуатируются больше 5 лет:");
        for (int i = 0; i < 10; i++) {
            cars[i].presetMarYear();
        }
        System.out.println("-список автомобилей 2001 года выпуска, цена которых больше 18000:");
        for (int i = 0; i < 10; i++) {
            cars[i].priceCar();
        }
    }
}

