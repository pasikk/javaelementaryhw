package lessons7_HW6.Car;

public class Car implements CarInterface {

    private int price;
    private int fuel;
    private int maxSpeed;

    public Car(int price, int fuel, int maxSpeed) {
        this.price = price;
        this.fuel = fuel;
        this.maxSpeed = maxSpeed;
    }

    public Car(int price, int maxSpeed) {
        this(price, 0, maxSpeed);
    }

    @Override
    public int getPrice() {
        return price;
    }

    @Override
    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public int getFuel() {
        return fuel;
    }

    @Override
    public void setFuel(int fuel) {
        this.fuel = fuel;
    }

    @Override
    public int getMaxSpeed() {
        return maxSpeed;
    }

    @Override
    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    @Override
    public String toString() {
        return "Car{" + "price=" + price + ", fuel=" + fuel + ", maxSpeed=" + maxSpeed + '}';
    }
}
