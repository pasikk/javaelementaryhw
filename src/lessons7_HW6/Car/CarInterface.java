package lessons7_HW6.Car;

interface CarInterface {

    int getPrice();

    void setPrice(int price);

    int getFuel();

    void setFuel(int fuel);

    int getMaxSpeed();

    void setMaxSpeed(int maxSpeed);
}
