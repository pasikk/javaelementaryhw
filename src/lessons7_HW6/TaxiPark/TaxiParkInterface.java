package lessons7_HW6.TaxiPark;

import lessons7_HW6.Car.Car;

public interface TaxiParkInterface {

    int coastCar();

    void sortFuel();

   Car[] findCarsBySpeedRange(int minSpeed, int maxSpeed);
}
