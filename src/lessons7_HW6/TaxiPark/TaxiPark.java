package lessons7_HW6.TaxiPark;

import lessons7_HW6.Car.Car;

public class TaxiPark implements TaxiParkInterface {

    private Car[] cars;

    public TaxiPark(Car[] cars) {
        this.cars = cars;
    }


    @Override
    public int coastCar() {
        int coast = 0;

        for (Car car : cars) {
            coast += car.getPrice();
        }
        return coast;
    }

    @Override
    public void sortFuel() {
        for (int i = cars.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (cars[ j ].getFuel() > cars[ j + 1 ].getFuel()) {
                    Car temp = cars[ j ];
                    cars[ j + 1 ] = temp;
                }
            }
        }
    }

    @Override
    public Car[] findCarsBySpeedRange(int minSpeed, int maxSpeed) {
        Car[] res = {};

        for (Car car : getCars()) {
            if (car.getMaxSpeed() >= minSpeed && car.getMaxSpeed() <= maxSpeed) {
                res = extendCarsArrayByNewCar(car, res);
            }
        }
        return res;
    }

        private Car[] extendCarsArrayByNewCar(Car carToAdd, Car[] cars) {
        if (cars.length == 0) {
            return new Car[]{carToAdd};
        } else {
            Car[] newCarArr = new Car[ cars.length + 1 ];

            for (int i = 0; i < cars.length; i++) {
                newCarArr[ i ] = cars[ i ];
            }
            newCarArr[ cars.length ] = carToAdd;
            return newCarArr;
        }
    }

    public Car[] getCars() {
        return cars;
    }

    public void setCars(Car[] cars) {
        this.cars = cars;
    }

    @Override
    public String toString() {
        String result = "";

        for (Car car : cars) {
            result += (car.toString() + "\n");
        }
        return result;
    }
}

