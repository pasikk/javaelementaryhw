package lessons7_HW6.TypesOfCars;

import lessons7_HW6.Car.Car;

public class ElectricCar extends Car {

    private int powerReserve;

    public ElectricCar(int price, int maxSpeed, int powerReserve) {
        super(price, maxSpeed);
        this.powerReserve = powerReserve;
    }

    public int getPowerReserve() {
        return powerReserve;
    }

    public void setPowerReserve(int powerReserve) {
        this.powerReserve = powerReserve;
    }

    @Override
    public int getFuel() {
        return 0;
    }

    @Override
    public String toString() {
        return "ElectricCar{" + "powerReserve=" + powerReserve + ", price=" + getPrice()
                + ", maxSpeed=" + getMaxSpeed() + '}';
    }
}
