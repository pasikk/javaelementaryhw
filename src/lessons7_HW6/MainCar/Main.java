package lessons7_HW6.MainCar;

import lessons7_HW6.TypesOfCars.Bus;
import lessons7_HW6.Car.Car;
import lessons7_HW6.TaxiPark.TaxiPark;
import lessons7_HW6.TypesOfCars.ElectricCar;
import lessons7_HW6.TypesOfCars.Truck;


import java.util.Arrays;

class Main {
    public static void main(String[] args) {

       Car[] cars = {new Bus(12000, 20, 100,  35),
                     new Bus(8000, 25, 80,  40),
                     new Bus(9000, 17, 75,  65),
                     new Bus(10000, 20, 60,  33),
                     new Bus(7500, 15, 140,  45),
                     new Truck(12000, 20, 100,  3500),
                     new Truck(8000, 25, 120,  4000),
                     new Truck(15000, 30, 80,  6000),
                     new Truck(9000, 17, 150,  6500),
                     new Truck(10000, 20, 70,  3300),
                     new ElectricCar(20000,  80,  350),
                     new ElectricCar(15000,  110,  350),
                     new ElectricCar(9000,  150,  320),
                     new ElectricCar(10000,  130,  320),
                     new ElectricCar(7500,  100,  320)};

       TaxiPark tp = new TaxiPark(cars);
       System.out.println(tp.coastCar());
       System.out.println();

       System.out.println(tp);
       System.out.println();

       tp.sortFuel();
       System.out.println(tp);

       System.out.println(Arrays.toString(tp.findCarsBySpeedRange(90, 130)));
       System.out.println(tp.findCarsBySpeedRange(90, 130));
    }
}
